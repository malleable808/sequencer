#!/bin/sh
PURE_DATA_STARTUP_SLEEP=5

PATCH="malleable808_sequencer.pd"

try_aconnect() {
	aconnect "$1" "$2" 2>/dev/null && echo connected $1 '-->' $2 || true
}

echo Killing all existing pure data instances and decoupling all midi connections.
aconnect -x
killall puredata
sleep $PURE_DATA_STARTUP_SLEEP

echo "Starting PD"
puredata -alsa -audioadddev pisound -alsamidi -channels 2 -r 44100 -mididev 1,2,3,4,5,6,7,8,9,10 -noverbose "$PATCH" $@ &
echo Giving $PURE_DATA_STARTUP_SLEEP seconds for Pure Data to start up before connecting MIDI ports.
sleep $PURE_DATA_STARTUP_SLEEP

echo Connecting MIDI ports
try_aconnect "pisound":0 "Pure Data":0  # connect pisound -> pd
try_aconnect "Pure Data":10 "pisound":0  # pd out to pisound

try_aconnect 24 "Pure Data":1   # Launchpad 1 (LHS) -> pd
try_aconnect "Pure Data":11 24  # pd -> Launchpad 1

try_aconnect 28 "Pure Data":2   # Launchpad 2 (RHS) -> pd
try_aconnect "Pure Data":12 28  # pd -> Launchpad 2

try_aconnect 32 "Pure Data":3   # Launch Control 1 -> pd
try_aconnect "Pure Data":13 32  # pd -> Launch Control 1

try_aconnect 36 "Pure Data":7   # Launchpad 3 (LHS) -> pd
try_aconnect "Pure Data":17 36  # pd -> Launchpad 3

try_aconnect 40 "Pure Data":8  # Launchpad 4 (RHS) -> pd
try_aconnect "Pure Data":18 40  # pd -> Launchpad 4

try_aconnect 44 "Pure Data":9  # Launch Control 2 -> pd
try_aconnect "Pure Data":19 44  # pd -> Launch Control

#python midi_io/poll_arrays.py &
aconnectgui
