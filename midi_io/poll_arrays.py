import schedule
import time
import os 

def poll_arrays():
    d = {directory:[0]*15 for directory in ["01","02","03","04","05","06"]} 
    # eventually we'd iterate over stringified 1-6 plus an offset for the position in setlist. 
    for directory in ["01","02","03","04","05","06"]: 
        files = os.listdir("midi_io/data/{directory}/PTN".format(directory=directory)) 
        for f in files: 
            if "trig" in f:
                idx = int(f.split("trig")[0].replace("s","")) - 1 
                d[directory][idx] = 1
        filename = 'midi_io/data/{directory}_arrays_exist.mseq'.format(directory=directory) 
        with open(filename, mode="w") as fd: 
            for dummy in d[directory]: 
                fd.write("{}\n".format(dummy))

# is this too often?
schedule.every(1).seconds.do(poll_arrays)

while True:
    schedule.run_pending()
    time.sleep(1)
