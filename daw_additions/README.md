### BPM detector

This is a simple max for live that takes the BPM from ableton and outputs it as a cc message, which is received by the MSEQ and used for duty and other timing calculations.

### Launchcontrol sysex

Users 1-4 on our launchcontrols are set up using these templates. Apply these templates to your launchcontrol using the launchcontrol editor available [here](https://resource.novationmusic.com/support/product-downloads?product=Launch+Control+XL) to be sure the controller is mapped to the sequencer correctly.
