# MSEQ
![A sequencer, and my hands with moody lighting](https://gitlab.com/malleable808/sequencer/uploads/eb46c69396646def04ee2be987fb31d8/live.jpeg)

A midi sequencer, written in Pure Data by the lovely folk at [Modular Devices Malleable Audio](https://modulardevicesmalleableaudio.github.io/mdma/documentation/MSEQ/manual). Documentation, explanation, usage demos, and the like can be found [here](https://malleable-808-sequencer.readthedocs.io), and music written using the sequencer is available [here](https://www.soundcloud.com/malleable808).

## Intro
We’re proud to present our open-source, hackable multiplayer Midi sequencer, which we use as the heart of our improvised live performances. We call it the MSEQ, and it provides 12 polyphonic midi sequencers and 5 drum midi sequencers, allowing us to collaboratively control a small orchestra of synthesisers over 14 channels of midi!

The set up involves two novation launchpads per person, and 2 novation launchcontrols for macro level controls, with each player having an independent view and control of what is going on. It allows for writing patterns into a 16 step grid, with probabilistic and conditional triggers, velocity, duty, and CC sequencing and modulation, chord progression, arps, and polyrhythmic note modulation. We hope you enjoy using it!

## Hardware setup

The sequencer uses a Raspberry Pi 4 with a [Blokas PiSound](https://blokas.io/) soundcard. It is built to work with the novation launchpad MK2, and launchcontrol XL (as at version 1.1 you can use two pairs of launchpads and two launchcontrols). The MIDI and CC messages are routed from the pi, (in our case currently through ableton, but there are many possible options) to control a range of synthesisers and drum machines, and in-the-box effects inside ableton.

### MIDI setup
The sequencer currently has 10 virtual midi ports set up, although we have streamlined some of the hardware we use in the live set, so not all of the ports are in use. This setup reflects how we like it, but you could change it to suit your needs. To run these virtual midi ports it is reccomended that you use `aconnect` or `aconnectgui`, or edit the script `start_MSEQ.sh`. 

`start_MSEQ.sh` determines both the midi latency (used to tightly sync with ableton) and the order of connection for one of two setups when run. Because either of the two setups contain some equipment that has the same name (2 * launchcontrol xl and 4 * launchpad mk2), the order of connection is important. To get around this we have found that a midi hub with power switches such as [this](https://www.amazon.co.uk/gp/product/B01A56ZNAU/) work well.

**Midi in**
- Port 1: pisound (or usb midi in) - clock sync from ableton
- Port 2: LP1 left hand launchpad mk2 (17)
- Port 3: LP2 right hand launchpad mk2 (33)
- Port 4: LC1 launchcontrol xl (U1:64 / U2:63 / U3:62 / U4:61)
- Port 5: *currently unused*
- Port 6: *currently unused*
- Port 7: *currently unused* 
- Port 8: LP3 second left hand launchpad mk2 (113)
- Port 9: LP4 second right hand launchpad mk2 (129)
- Port 10: LC2 second launchcontrol xl (U1:160 / U2:159 / U3:158 / U4:157)

**Midi out**
- Port 1: pisound (or usb midi out) - channels 1-16 to ableton
- Port 2: LP1 left hand side launchpad (17)
- Port 3: LP2 right hand side launchpad (33)
- Port 4: LC1 launchcontrol xl (U1:64 / U2:63 / U3:62 / U4:61)
- Port 5: *currently unused*
- Port 6: *currently unused*
- Port 7: *currently unused*
- Port 8: LP3 second left hand launchpad mk2 (113)
- Port 9: LP4 second right hand launchpad mk2 (129)
- Port 10: LC2 second launchcontrol xl (U1:160 / U2:159 / U3:158 / U4:157)
