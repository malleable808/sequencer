## Changelog
#### Coming soon

### 2022
#### February 2022
- v2.7 Chord progression modulation
	- Chord Progression modulation (allowing per sequencer, per bar chord progression modulation)
	- Easier shortcuts for triggering inverse mod chance triggers (eg 1,2,3 but not 4)

### 2021
#### October 2021
- v2.6 Autodrops and Launchcontrol updates
	- Manual autodrop controls
		- muting & re-cuing using `rec-arm` + bottom LC buttons (whichever is next - an 8-bar, 4-bar, or 1-bar autodrop).
		- cueing an autodrop midi toggle using `shift`+`solo` and the bottom LC buttons for high-pass filter.
		- cueing an autodrop midi CC trigger using `shift`+`rec.arm` and the bottom LC buttons.
	- Front end for autodrops - LC buttons coloured to show autodrop status (manually triggered or autodrop triggered)
	- Volca sample velocity modulation.
	- Hardware sends FX view on the Launchcontrol user 5 & 6
	- Launchcontrol faders control volume per synth
	- Launchcontrol knobs control velocities
	- Fixes for a range of things:
		- Half-speed / Quarter Speed mod chance / inverse mod chance
		- improved chord progression timing
		- Velocity arp timing
		- Octave sequencer timing

#### May 2021
- v2.5 Velocity and Duty arps
	- Volca Sample integration (3rd Drum machine)
	- Velocity arps (scale velocity per row per sequencer)
	- Duty arps (scale duty per row per sequencer)
	- Local off toggle on clock division view

#### March 2021
- v2.4 Autodrops, swing, inverse mod chance
	- Coarse and fine control for Swing, per sequencer
	- Added a probabilistic autodrop module that randomly affects selected sequencers in the last bar of a 16, or on the 8th or 12th bars.
	- Mute Autodrops for each sequencer, and individual drum voices
	- Midi Note, and CC sweeps for each sequencer and drum input channel, and for master FX
	- Inverse mod chance trigger conditions (eg 1,2,3 but not 4)

#### February 2021
- v2.3 Timing fixes

#### January 2021
- v2.2 Arps, Chords & automated drum muting
	- New Arp UI and arp presets.
    - Removed unnecessary latency compensation.
	- UI improvements when switching views, editing the step that A/B chord progressions change on
	- 2 button combinations for Launchcontrol buttons
	- Sleep button to turn lights off (and back on again)
	- Boom-tsk timing fixes
	- Drum autodropping (probabilistic muting of active drum elements in the 15th bar)
	- Chord inversions (1st / 2nd inversion, random choice, random chance)
	- Rootnote and Minor/Major Scale UI
	- Row octave for chord progression
	- Row octave meta-sequencing 
	- Clear sequencer by row with SHIFT + MUTE + row mute button
	- Value interpolation (smoothing) for CC sequencing
	- Jam keyboards / gated pattern keyboards now working without lag

### 2020
#### December 2020
- v2.1 Legato & Drone additional (cue-able) row mute states.

- v2.0: Direct Monitoring, CC arps, efficiency gains on tied notes (the lockdown update)
	- Large scale rewiring of most of the main functions in the sequencer :boom:
	- set up for Ableton direct monitoring
	- Refactor all MSEQs and clocks for tighter sync
	- User 3 & 4 mapping of launchcontrol to ableton effects
	- User 3 mapping of 4 fx chains per channel
	- Tied notes available in all non-drum sequencers
	- CC arps across all synths
	- lighting triggers for live performance
	- A/B Chord progressions follow toggle per sequencer (A/B change on different step)
	- selectable per sequencer mod-wheel x2
	- Editable chord progression note modification
	- TR8 FX sequencing
	- Increased modularity of views (any view wherever you like)
	- Volca drum sequencer (including 2x tuned volca drum voice sequencers)
	- [Documentation](https://malleable-808-sequencer.readthedocs.io/en/latest/)
	- per sequencer trig delay (untested)
	- Experimental proof of concept direct monitoring sequenced keyboard (gated pattern for keyboard notes to play in time despite direct monitoring lag) 

#### January 2020
- v1.3: Tied notes and percentage chance
	- Note tying :tada:
	- Improved UI for drum tracks
	- Enqueue mutes (drop on 16)
	- Combination Launchcontrol toggle states
	- Percentage chance toggle state
	- Mod 1:2/2:2/4:4/8:8/16:16 toggle states
	- Clock division per sequencer (half and quarter speed)
	- Chord Progression follow toggle per sequencer
	- Arpeggiator presets

#### June 2019
- v1.2: Drum Machine sequencing & Sustain
	- Reorganised sequencers for live performance
	- Sustain
	- UI improvements (view colour scheme)
	- TR-8 view
	- TR-8 8 step velocity arps
	- Slicer view
	- Half speed on channel 2
	- Mute channels on the end of a bar / 16

#### May 2019
- v1.1: Four Launchpads, Saving arrays and Intellijel Metropolis
	- Multiplayer mode! 2 Launch Controls, 4 Launchpads
	- Saving array view allows for saving and hot-loading of up to 6 arrays per sequencer as text files
	- Added an assignable Intellijel Metropolis clone on sequencer 7
	- mute MIDI channels on LaunchControl using mute key
	- max4live BPM sent to puredata (better swing, duty)

#### March 2019
- v1.03: small fixes & user testing

- v1.02: "Moving in"
    - Updated positioner performance
    - enabled and set colour schemes per synth on the barcounter

- v1.01: CC sequencing, Ableton MIDI through, and quick fixes
	- Midi through for ableton control
	- Startup scripts with variable Midi setup and midi sync delay options.
	- Fixed timing issues
	- Arp light on the positioner as well as mute button
	- Chord modification - revoicing chords v0.1
	- Duty and velocity sequencing
	- CC sequencing over 8 CC messages in one view

#### February 2019
- v1.0: Some more things
    - Tight ableton sync over midi (:tada:!!!)
    - Arps work with chord progression
    - Trigger mutes

- v0.95: A lot of things.
	- Sequencer level octave shifting.
	- Sequencer octave shifting by row (a sequencer can play in-scale notes from multiple octaves)
	- Arp length view and arp editing using a keyboard to visualise arp notes.
	- Arp indicator on the mute lights
	- Beatcounter light along the top to see where you are when not in global note editing view.
	- Jamming ("push-style") keyboards with adjustable octave range. 
	- Launch control responsive ligthing for knobs and buttons.
	- Launch control octave shifting for sequencer and jam keyboards.
	- Shift button functionality

#### January 2019
- v0.92: Multiple Sequencers: Added working sequencers 1-15, with step velocity and duty views. Load Notes on global view per sequencer, per sequencer muting

- v0.91 : Refactor UI to have 15 sequencer Slots (+1 chord progression slot) across the top, and 8 views per slot along the RHS vertical set of buttons. Arps are not editable until next release, but arplength indicators on the mute buttons will highlight when arps are enabled.

### 2018
#### December 2018
- v0.9: implemented views. Per step duty and velocity, and Chord Progression view! View for CC41 for volca FM velocity sequencing (still front end only. Back end functionality to come next).
- v0.8: split mute and select into two columns, build to 16 steps (including bar counter), better colours (migration to launchpad mkII), rootnote shifting

#### November 2018
- v0.71: Bugfixes and quick wins: ableton sync improved, mutes send noteoff, scale loading and arp editing is quicker and more stable.
- v0.7: implement scales, ableton midi sync, global duty and velocity

#### October 2018
- v0.5: replaced janky sequencer with non-janky sequencer. also swing.
- v0.4: sequencer mute and select options added.
- v0.3: added arps!
- v0.2: added basic but janky sequencer with launchpad functionality
- v0.1: added previous sequencer files for reference / cannibalisation
